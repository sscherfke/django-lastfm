# encoding: utf-8
from StringIO import StringIO
import urllib

from django.core.urlresolvers import reverse
from django.test import TestCase
import mock

from lastfm import views


class ViewsTest(TestCase):

    @mock.patch('urllib.urlopen')
    @mock.patch.object(views, 'LASTFM_CHART_TYPE', 'recent_tracks')
    @mock.patch.object(views, 'LASTFM_IMG_SIZE', 'large')
    def test_lastfm_data(self, urlopen_mock):
        assert views.LASTFM_CHART_TYPE == 'recent_tracks'
        assert views.urllib.urlopen is urlopen_mock

        # Test last.fm error
        urlopen_mock.return_value = StringIO('{"error": "fu"}')
        with self.assertRaises(RuntimeError):
            self.client.get(reverse('lastfm'))

        # Test empty data
        urlopen_mock.return_value = StringIO('{"recenttracks":{"track":{}}}')
        response = self.client.get(reverse('lastfm'))
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.content, '[]')

        # Test only one item
        urlopen_mock.return_value = StringIO(
           '{"recenttracks":{"track":{"artist":{"#text":"Artist1"},'
           '"name":"Name1","url":"URL1","image":[{"#text":"small_img",'
           '"size":"small"},{"#text":"medium_img","size":"medium"},'
           '{"#text":"large_img","size":"large"},{"#text":'
           '"extralarge_img","size":"extralarge"}]}}}')
        response = self.client.get(reverse('lastfm'))
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.content, '[{"url": "URL1", "img_url":'
            ' "large_img", "title": "Artist1 \u2013 Name1"}]')

        # Test normal case
        urlopen_mock.return_value = StringIO(
            '{"recenttracks":{"track":[{"artist":{"#text":"Artist1"},'
            '"name":"Name1","url":"URL1","image":[{"#text":"small_img",'
            '"size":"small"},{"#text":"medium_img","size":"medium"},'
            '{"#text":"large_img","size":"large"},{"#text":'
            '"extralarge_img","size":"extralarge"}]},{"artist":'
            '{"#text":"Artist2"},"name":"Name2","url":"URL2","image":'
            '[{"#text":"small_img","size":"small"},{"#text":"medium_img",'
            '"size":"medium"},{"#text":"large_img","size":"large"},'
            '{"#text":"extralarge_img","size":"extralarge"}]}]}}')
        response = self.client.get(reverse('lastfm'))
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.content, '[{"url": "URL1", "img_url": '
                '"large_img", "title": "Artist1 \u2013 Name1"}, {"url": '
                '"URL2", "img_url": "large_img", "title": "Artist2 \u2013 '
                'Name2"}]')


class RecentTracksTest(TestCase):

    def setUp(self):
        self.charts = views.RecentTracks()
        self.test_data = {
            'recenttracks': {
                'track': [
                    {'artist': {'#text': 'Test'}, 'name': 'Foo'},
                ],
            },
        }

    def test_get_data(self):
        ret = self.charts.get_data(self.test_data)
        self.assertEqual(ret, self.test_data['recenttracks']['track'])

    def test_get_item_title(self):
        item = self.test_data['recenttracks']['track'][0]
        ret = self.charts.get_item_title(item)
        self.assertEqual(ret, u'%s – %s' %
                (item['artist']['#text'], item['name']))

    def test_get_default_image(self):
        ret = self.charts.get_default_image()
        self.assertEqual(urllib.urlopen(ret).code, 200)


class WeeklyTopArtistsTest(TestCase):

    def setUp(self):
        self.charts = views.WeeklyTopArtists()
        self.test_data = {
            'weeklyartistchart': {
                'artist': [
                    {'name': 'Test', 'playcount': '23'},
                ],
            },
        }

    def test_get_data(self):
        ret = self.charts.get_data(self.test_data)
        self.assertEqual(ret, self.test_data['weeklyartistchart']['artist'])

    def test_get_item_title(self):
        item = self.test_data['weeklyartistchart']['artist'][0]
        ret = self.charts.get_item_title(item)
        self.assertEqual(ret, '%s (%s plays)' %
                (item['name'], item['playcount']))

    def test_get_default_image(self):
        ret = self.charts.get_default_image()
        self.assertEqual(urllib.urlopen(ret).code, 200)

    @mock.patch('urllib.urlopen')
    def test_get_img_url(self, urlopen):
        item = self.test_data['weeklyartistchart']['artist'][0]

        # normal case
        json = ('{"images":{"image":{"sizes":{"size":[{"#text":'
                '"original_url","name":"original"},{"#text":"large_url",'
                '"name":"large"}]}}}}')
        urlopen.return_value = StringIO(json)
        ret = self.charts.get_img_url('large', item)
        self.assertTrue(urlopen.called)
        self.assertEqual(ret, 'large_url')

        # 'large' not in list
        json = ('{"images":{"image":{"sizes":{"size":[{"#text":'
                '"original_url","name":"original"}]}}}}')
        urlopen.return_value = StringIO(json)
        ret = self.charts.get_img_url('large', item)
        self.assertTrue(urlopen.called)
        self.assertEqual(ret, 'original_url')

        # 'large' not in list
        json = '{"images":{"image":{"sizes":{"size":[]}}}}'
        urlopen.return_value = StringIO(json)
        ret = self.charts.get_img_url('large', item)
        self.assertTrue(urlopen.called)
        self.assertEqual(ret, '')

        # Empty response from last.fm
        urlopen.return_value = StringIO('')
        ret = self.charts.get_img_url('large', item)
        self.assertTrue(urlopen.called)
        self.assertEqual(ret, '')


class TopArtistsTest(TestCase):

    def setUp(self):
        self.charts = views.TopArtists()
        self.test_data = {
            'topartists': {
                'artist': [
                    {'name': 'Test', 'playcount': '23'},
                ],
            },
        }

    def test_get_data(self):
        ret = self.charts.get_data(self.test_data)
        self.assertEqual(ret, self.test_data['topartists']['artist'])

    def test_get_item_title(self):
        item = self.test_data['topartists']['artist'][0]
        ret = self.charts.get_item_title(item)
        self.assertEqual(ret, '%s (%s plays)' %
                (item['name'], item['playcount']))

    def test_get_default_image(self):
        ret = self.charts.get_default_image()
        self.assertEqual(urllib.urlopen(ret).code, 200)
